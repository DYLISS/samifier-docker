---
title:  Samifier Reborn - Documentation développeur
author: Pierre VIGNET
date: 2016-03-01 - 2016-10-15
version: 2016-10-15
geometry: margin=2cm
---

                  ___                      _
                 |  _`\                   ( )
                 | (_) )   __     _ _    _| |  ___ ___     __
                 | ,  /  /'__`\ /'_` ) /'_` |/' _ ` _ `\ /'__`\
                 | |\ \ (  ___/( (_| |( (_| || ( ) ( ) |(  ___/
                 (_) (_)`\____)`\__,_)`\__,_)(_) (_) (_)`\____)

                        @author: Pierre VIGNET, March 2016

################################################################################

\newpage


# Docker

## Installation

Instructions of installation, depending of your distribution are available here:
[Instructions](https://docs.docker.com/engine/installation/).

Versions of docker **above 1.10** are supported.

You have to install [docker-compose](https://pypi.python.org/pypi/docker-compose/1.4.2)
if you want to use `docker-compose.yml` files.

    sudo pip3 install docker-compose
    or
    pip3 install --user docker-compose


## Architecture

    .
    ├── README.md
    └── image_making
        ├── docker-compose.yml
        ├── Dockerfile
        ├── launch_container.sh
        ├── samifier.bin
        ├── logs
        │   ├── db
        │   │   ├── postgresql-2016-04-09_123221.log
        │   │   └── postgresql.log
        │   └── web
        │       ├── access.log
        │       ├── error.log
        │       ├── gunicorn.log
        │       └── php5-fpm.log
        ├── Makefile
        └── server_configs
            ├── default
            ├── docker-entrypoint-initdb.d
            │   ├── 00_public.sql
            │   ├── 01_text_matching.sql
            │   └── 03_pg_similarity.sql
            ├── docker-entrypoint.sh
            ├── Makefile
            ├── postgresql.conf
            ├── requirements.txt
            ├── unifier
            │   ├── biological_object.metadata.json.gz
            │   └── biological_object.bson.gz
            └── supervisord.conf


- `image_making`: This directory is used by developers during the creation of an empty image.



## Documentation

If you want to generate a proper PDF of this readme, you will have to install the following system packages:

    pandoc texlive-xetex

and then, please execute `make` in the root of Docker project, where the file `README.md` is.


## Image making


The Dockerfile is used by `docker-compose` (with its configuration file: `docker-compose.yml`)
to generate the final image.

Files in `server_configs` are copied in this image. These files are configuration
files of all servers and applications:

- `default`: Nginx configuration (web server + webDAV server).
- `docker-entrypoint-initdb.d`: files which are loaded in SQL database during its initialization (They are ordered).
- `docker-entrypoint.sh`: Initialization script of PostgreSQL database. This script is widely copied from the official Dockerfile of PostgreSQL.
- `Makefile`: Makefile used to facilitate installation of git repositories in the empty image.
- `postgresql.conf`: PostgreSQL configuration.
- `requirements.txt`: Python requirements from pypi.
- `supervisord.conf`: Configuration of supervisor daemon. This daemon is in charge of automatic launch of services in the container.
- `unifier`: Dump of MongoDB database. This dump is used to accelerate the initialization of the database (1 second vs 15minutes).
    You can use the command `make init_mongo_db` in the project to regenerate the DB yourself.

During the initialization of the image, **some** services are launched (Nginx, PostgreSQL, PHP-fpm, Mongo).
Log files are stored in `logs` folder.


### Make an empty image (for mainteners or developers)


- Please clone the current repository (`git clone --branch reborn https://gitlab.inria.fr/DYLISS/samifier-docker.git`)

- Make sure you have docker and docker-compose installed on your system (`pip install docker-compose`).
- Build & run the image for the first time in the folder `image_making`:

    ```
    make build
    ```

- At the end of the process Samifier image is ready to be distributed.

    ```
    $ docker images
    REPOSITORY      TAG      IMAGE ID       CREATED          SIZE
    samifier_reborn     0.1.1              7331e9eea516        About an hour ago   1.835 GB
    ```

- Save the image to a compressed archive (directly saved in the current directory) for its sharing:

    ```
    make save
    ```

    **or**:

    ```
    $ docker save samifier_reborn | gzip > samifier.bin
    -rwxrwxrwx 1 root root 318587585 avril  9 14:47 samifier.bin
    ```

    or with multithreaded LZMA compression:

    ```
    $ docker save samifier_reborn | pxz > samifier.tar.xz
    -rwxrwxrwx 1 root root 221856808 avril  9 14:46 samifier.tar.xz
    ```

At the end of this process, samifier.bin may be distributed.
If you want to work into this container as a developer, you can stay in the current folder
and use advanced directives stored in the Makefile.

Example of directives:

    <make help>      Display this help."
    <make version>   Display informations about the image."
    <make all>       Build and run the image in new container."
    <make build>     Build the image."
    <make run>       Run the image in new container."
    <make enter>     Enter in the current container as root."
    <make save>      Save the image to an archive."
    <make load>      Load the archive."
    <make clean>     Remove containers, backups, logs, repositories."
    <make list>                  List all containers and volumes."
    <make list_all_containers>   List all containers."
    <make list_volumes>          List all volumes."
    <make backup_all>            Do backup of databases and files uploaded."
    <make restore_all>           Do a restoration of all data previously backed up."
    <make remove_container>      Remove $(IMAGE_NAME)_cont from the system except backups."
    <make remove_container_and_volume> Remove the container and the backups."
    <make list_volumes>          List backup volumes on the system."
    <make container_info>        Inspect samifier container."

You will be advised not to develop **in** the container but in a directory mounted in it.
For that, you can uncomment these 3 lines in `docker-compose.yml` file:

    ##- ./samifier-web:/root/samifier-web
    ##- ./samifier-matching-kernel:/root/samifier-matching-kernel
    ##- ./samifier-matching-kernel_doc:/root/samifier-matching-kernel_doc

These 3 folders will be bind in the current folder.

- samifier-web: Symfony git project (PHP).
- samifier-matching-kernel: Matching kernel git project (Python).
- samifier-matching-kernel_doc: Documentation for developers (accessible through the web interface, logged as admin)


## Image running (for users)

- Please clone the current repository (`git clone --branch reborn https://gitlab.inria.fr/DYLISS/samifier-docker.git`)

- Put the archive in the folder `image_making` and load it to Docker service:

    ```
    make load
    ```

    **or**:

    ```
    $ docker load < samifier.bin
    ```

    **Please note that you can't have an existing image of the same name !** (samifier_reborn)
    => Please check Docker documentation about `docker rmi image_id`.

- Run & make a new container with this image:

    ```
    $ docker-compose up --no-recreate
    or :
    $ chmod +x launch_container.sh && ./launch_container.sh
    or :
    make run
    ```

    **Please note that you can't have an existing container of the same name !**
    => Please check Docker documentation about `docker rm container_id`.

    **If you don't want to loose your changes** in the container, don't forget the `--no-recreate` argument !

    Note: The previous command is the same as this cli-command for Docker:

    ```
    $ docker run -it -p 8888:80 -p 5432:5432 -p 8080:8080 --name samifier_reborn_cont samifier_reborn supervisord -c /etc/supervisor/supervisord.conf
    ```


- Once the installation process is done, your samifier instance is ready to be used.

    The platform is now up and the supervisor should'nt mention unexpected stop of daemons that it manages.
    If this is the case it is that there is a problem that can be debugged as follows:

     From the container:
     `/var/log/nginx/`

     Or from the host:
     `/root/samifier-matching-kernel/logs/`

     Or from the host machine (or VM host) from the folder hosting the project Docker:
     `samifier-docker/image_making/logs/`


## Use Samifier

### Access via webDAV

Web Distributed Authoring and Versioning (WebDAV) is an extension of the Hypertext Transfer Protocol (HTTP)
that allows clients to perform remote Web content authoring operations.
The WebDAV protocol provides a framework for users to create,
change and move documents on a server, typically a web server.

URI: `webdav://127.0.0.1:8080/`

Configuration example with Dolphin on KDE systems:

![Step 1 - WebDAV configuration](readme_ressources/webdav1.png "webdav config")
![Step 2 - WebDAV configuration](readme_ressources/webdav2.png "webdav config")


### Access via FTP over SSH (best solution)

**/!\\ not implemented yet !**

Secure File Transfer Protocol (SFTP) uses interactive FTP session over SSH protocol.
Like webDAV, it provides an access to a network folder, but securely,
and with respect of the user rights instead of the web server limited rights.

URI: `sftp://pi@127.0.0.1:2222/`

Configuration example with Dolphin on KDE systems:

![Step 1 - SFTP configuration](readme_ressources/sftp1.png "sftp config")
![Step 2 - SFTP configuration](readme_ressources/sftp2.png "sftp config")


### Access via your web browser

Samifier is a web platform. Once the docker container is launched you can access to it
via your web browser.

#### Urls

For this you will need to modify your hosts file and add this line:

    127.0.0.1   samifier.inria.fr

Localization of your hostfile:

- On Windows: `C:\Windows\System32\drivers\etc\hosts`
- On GNU/Linux: `/etc/hosts`
- On MacOS: `to be completed`

NB: If you do not to want or can not modify this file, you will have to
modify the Nginx config file like this:

    Comment this line:
        server_name samifier.inria.fr;

    like this:
        #server_name samifier.inria.fr;

#### URLS for Dev and Prod

Note: **The web server port is 8888 (http://127.0.0.1:8888/)**.

* The development environment is here: `http://samifier.inria.fr:8888/app_dev.php`
* The production environment is here: `http://samifier.inria.fr:8888/`


#### Accounts

Two accounts are created by default on the platform:

    login: admin; password: pass
    login: user; password: pass


### Logging

Logs are accessible from the host machine folder:

    docker/Running_image/logs/web/

These logs can be viewed by anyone because of the directive `RUN chmod -R 777 /var/log`
in the Dockerfile.

Please take note of this for developing a production environment...

PS: On a recent GNU/Linux system with Systemd, you can access to container logs with:

    sudo journalctl CONTAINER_NAME=samifier_cont

If you haven't Systemd on you system, you will have to modify logging directive in `docker-compose.yml`.



### Work on Mac OS

It seems that it is possible!
Here is the list of changes to be applied:


- You need to work in a Virtual Machine. Working with Docker here is more secure than under Linux
systems where containers can be run with root privileges;

- You must update the file `/etc/hosts` in the container with the ip address of your Virtual Machine.

    Example:

        192.168.99.100    samifier.inria.fr


\newpage

# Architecture du backend Python

## Installation

System dependencies:

    libpq-dev

Python dependencies:

    pip3 install -r requirements.txt

Architecture:

`tree -I *pyc*`

    .
    ├── data
    │   ├── all_metabolites.tbl
    │   ├── all_reactions.tbl
    │   ├── chem_xref1.0.tsv
    │   ├── chem_xref1.1.tsv
    │   ├── chem_xref2.0.tsv
    │   ├── html_entities.txt
    │   ├── metacyc_html_entities.txt
    │   ├── metacyc_html_false_entities.txt
    │   ├── reac_prop.tsv
    │   ├── reac_xref.tsv
    ├── doc
    │   └── sphinx_doc
    │       ├── logs
    │       │   └── matching_kernel.log
    │       ├── matching_kernel.log
    │       └── source
    │           └── _templates
    ├── doc.tar.bz2
    ├── logs
    │   ├── matching_kernel.log
    │   └── matching_kernel.log.1
    ├── Makefile
    ├── matching_kernel
    │   ├── commons.py
    │   ├── convert
    │   │   ├── convert.py
    │   │   ├── elementnames_handling.py
    │   │   └── __init__.py
    │   ├── info.py
    │   ├── __init__.py
    │   ├── input_file_parser
    │   │   ├── check_integrity.py
    │   │   ├── __init__.py
    │   │   └── populate_database.py
    │   ├── __main__.py
    │   ├── matching
    │   │   ├── exact_identifier_matching.py
    │   │   ├── exact_name_matching.py
    │   │   ├── get_in_validation.py
    │   │   ├── inferred_name_matching.py
    │   │   ├── __init__.py
    │   │   └── samir.py
    │   ├── output_file_writer
    │   │   ├── __init__.py
    │   │   └── writer.py
    │   ├── reference_database
    │   │   ├── import_metacyc.py
    │   │   ├── import_metanetx.py
    │   │   ├── __init__.py
    │   │   ├── main_importer.py
    │   │   ├── mongo_wrapper2.py
    │   │   ├── mongo_wrapper.py
    │   │   └── sparql_wrapper.py
    │   ├── rest_api.py
    │   ├── switch_id_name.py
    │   ├── test
    │   │   ├── __init__.py
    │   │   └── test_convert.py
    │   ├── unifier_database
    │   │   ├── __init__.py
    │   │   └── sqla_wrapper.py
    │   └── utils
    │       ├── colours_in_the_shell.py
    │       ├── easy_pickle.py
    │       └── __init__.py
    ├── matching_kernel.e4p
    ├── README.md
    └── requirements.txt

## Root folder

- `matching_kernel.e4p`: Project file used by Eric IDE
- `README.md`: This file
- `requirements.txt`: List of packages required by the whole project
- `Makefile`: Useful commands
- `doc.tar.bz2`: A copy of the documentation generated by Sphinx autodoc

## Doc folder

This directory is updated with the command `make updatedoc` on the branch `doc` of the project.
The content is cloned on the webserver during the installation of samifier.

The following Python dependencies are necessary (take a look in the file `requirements.txt`):

    Sphinx==1.3.6
    sphinxcontrib-httpdomain==1.5.0
    sphinxcontrib-mockautodoc

The full documentation is avalaible for administrators on Samifier at `http://localhost/documentation/`.

## Data folder & Database initialization

Files used to intitialize the reference database.

- `all_metabolites.tbl` (used): Dump of metabolites in the tgdb based on MetaCyc; version XX;
Headers: `METACYC    SYNONYMS    XREF`.
cf [Gestion des fichiers utilisateurs](#gestion-des-fichiers-utilisateurs).
- `all_reactions.tbl` (used): Dump of reactions in the tgdb based on MetaCyc; version XX;
Headers: `METACYC    SYNONYMS    XREF    REAGS   PRODS   REV`.
cf [Gestion des fichiers utilisateurs](#gestion-des-fichiers-utilisateurs).
- `chem_xref1.0.tsv` (not used): MetaNetx dump of the descriptions & cross references of metabolites; version 1.0; version 14/06/2013; MetaCyc 17.0
- `chem_xref1.1.tsv` (not used): MetaNetx dump of the descriptions & cross references of metabolites; version 1.1; version 2015/04/17; MetaCyc 17.0
- `chem_xref2.0.tsv` (used): MetaNetx dump of the descriptions & cross references of metabolites; version 2.0; version 2015/09/03; MetaCyc 19.1
- `html_entities.txt`: cf [Module convert](#module-convert-et-tests-unitaires-sur-les-fonctions-de-decodage-encodage-didentifiants)
- `metacyc_html_entities.txt`: cf [Module convert](#module-convert-et-tests-unitaires-sur-les-fonctions-de-decodage-encodage-didentifiants)
- `metacyc_html_false_entities.txt`: cf [Module convert](#module-convert-et-tests-unitaires-sur-les-fonctions-de-decodage-encodage-didentifiants)
- `reac_prop.tsv` (used): MetaNetx dump of the descriptions of reactions; version 2015/09/03; MetaCyc 19.1
- `reac_xref.tsv` (used): MetaNetx dump of the cross references of reactions; version 2015/09/03; MetaCyc 19.1

### Preprocessing

`all_metabolites` and `all_reactions.tbl` are cleaned during the init process by the command `make clean_metacyc_dump`.
It is because of MetaCyc's mess; some ids have to be renamed or cleaned by removing bad separators.

Some ids contain the separator `:`. It causes some problems because this is also the choosen separator for the tabulated file...
Moreover 5 metabolites have to be renamed: OCTADECNUMBER:9-ENE-118-DIOIC-ACID, L-seryl-SECNUMBER:tRNAs, SECNUMBER:tRNAs, POLY-OH-DECNUMBER:N, Charged-SECNUMBER:tRNAs,

into: OCTADEC-9-ENE-118-DIOIC-ACID, L-seryl-SEC-tRNAs, tRNA-Sec POLY-OH-DEC-N, Charged-SEC-tRNAs.

These metabolites are involved in 6 reactions: RXN-2122, RXN0-2161, RXN-11796, RXN-10039, RXN-10038, 2.9.1.1-RXN

If the process of insertion of the reactions doesn't find metabolites in database inconsistencies will be outputted.

Example of inconsistencies:

    ERROR: Inconsistencies in reagents: set()defaultdict(<class 'list'>, {'ECNUMBER': ['2.9.1.1'], 'KEGG': ['R08219'], 'UNIPROT': ['O26043', 'P0A821', 'O67140', 'Q9HV01', 'Q9PMS2', 'P43910'], 'METACYC': ['2.9.1.1-RXN']})
    ERROR: Expected ids: {'L-seryl-SECNUMBER:tRNAs', 'SEPO3'}
    ERROR: Present ids: [['SEPO3']]


### Initialization of the objects

The command `make init_db` initializes the database with structure and objects handled by SQLAlchemy.
Here we init the database with default objects that are fully supported by sqlalchemy
(Step, Status, Origin, ResourceFile, Reaction, Metabolite, Synonym, Association).

Take a look at `commons.py`; you will see all these global static parameters:

    AUTH_STATUS             = ("already matched", "proposal", "to be matched", "cancelled", "validation")
    AUTH_STEPS              = ("exact name", "word", "formula", "samir", "exact id")
    AUTH_ORIGINS            = ("metacyc", "bigg", "kegg", "user dst", "user src",
                            "chemspider", "metanetx", "chebi", "inchikey", "smiles")
    PREFIXES_ORIGINS        = {"metacyc": "http://biocyc.org/META/new-image?object=",
                            "bigg": "http://bigg.ucsd.edu/universal/metabolites/",
                            "kegg": "http://www.genome.jp/dbget-bin/www_bget?",
                            "chemspider": "",
                            "metanetx": "http://identifiers.org/metanetx.chemical/",
                            "chebi": "http://identifiers.org/chebi/CHEBI:",
                            "inchikey": "",
                            "smiles": "",
                            }
    STA_AM, STA_PROP, STA_TBM, STA_CANC, STA_VA = AUTH_STATUS
    STE_EX, STE_WO, STE_FO, STE_SAMIR, STE_EX_ID = AUTH_STEPS
    O_MET, O_BIG, O_KEGG, O_USRDST, O_USRSRC, \
    O_CHEMSPIDER, O_METNTX, O_CHEBI, O_INCHI, \
    O_SMILES = AUTH_ORIGINS

If you want to add/remove the support of external databases, you will have to modify
`AUTH_ORIGINS` + `PREFIXES_ORIGINS` (and reload `make init_db`).
This will impact the list of databases proposed during the creation of an experiment.

Moreover, we create here the "Experiment 0". It's the first experiment in the database.
This experiment is reserved to the first admin of the system and will memorize
the list of matches validated by users and by the administrators.

Note: This command must be called before `make pinput`.


### Bulk insertions

The command `make pinput` loads compounds & reactions from raw files (metanetx.org, MetaCyc)
in the database.

Please take a look at paragraph [Reference database](#reference-database) for more information on the Python module
that makes this work.

Note: This command must be called after `make init_db`.


## Serveur WSGI

Nginx passe par un serveur intermédiaire ([Gunicorn](http://gunicorn.org/))
dédié à l'éxécution du code Python. En production, l'API est accessible via Nginx sur le port 4000 et
n'est jamais exposée au public pour des raisons de performances et de sécurité.


La commande `make dev_flask_start` actuellement utilisée,
lance un serveur de **développement** sur `127.0.0.1:5000`.


La commande de lancement de Gunicorn en **production** est la suivante :

    gunicorn \
        -c /root/samifier-matching-kernel/matching_kernel/gunicorn.conf.py \
        --access-logfile /var/log/nginx/mk_access.log \
        --error-logfile /var/log/nginx/mk_error.log \
        --umask 0007 \
        matching_kernel.rest_api:app

avec:

- `rest_api.py`:le point d'entrée du serveur;
- `matching_kernel.sock` le socket d'écoute.
- `gunicorn.conf.py`: le fichier de configuration de gunicorn

NB: Cette commande est placée dans la configuration du superviseur `/etc/supervisor/conf.d/supervisord.conf`.
NB 2: Nginx écoute le socket `/run/matching_kernel.sock` (cf la config ci-dessous).


La configuration de Nginx en tant que proxy pour l'API est la suivante:

    server {

            listen 4000;

            # YES, this folder doesn't exist
            # So Nginx will never find any file guessed by a user
            # ie : Avoid dump of any file on server through the api
            root /var/www/html/api/;

            location / {
                # By default, Gunicorn will only trust these headers if the connection comes from localhost.

                # enable this if and only if you use HTTPS
                # proxy_set_header X-Forwarded-Proto https;
                proxy_set_header   Host             $http_host;

                # proxy_set_header   X-Real-IP         $remote_addr;
                proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Proto $scheme;

                # we don't want nginx trying to do something clever with
                # redirects, we set the Host: header above already.
                proxy_redirect     off;

                # For now, try_file will always fails to find any file (due to the fake root directory)
                try_files $uri @backend;
            }

            location @backend {
                proxy_pass http://flask_server;
            }
    }
    upstream flask_server {
            # swap the commented lines below to switch between socket and port
            # You can enable load balancing by specifying multiple servers and adding weights on them.
            # Prod
            server unix:/run/matching_kernel.sock weight=1000 fail_timeout=0;
            # Dev
            server 127.0.0.1:5000 fail_timeout=0;
    }


## Projet matching_kernel

C'est le cœur logique du projet. Le serveur Web interroge l'API de ce noyau pour le moindre calcul et
la moindre vérification de l'intégrité des fichiers mis en ligne par les utilisateurs.

Ces rôles ne doivent en aucun cas être dévolus au serveur Web et/ou au langage PHP qui
ne sont pas conçus pour.


### Paramètres globaux

Tous les paramètres de l'application sont regroupés dans le fichier `commons.py`.
Une partie a déjà été expliquée dans le chapitre [Initialization of the objects](#initialization-of-the-objects),
on y retrouve de plus:

- les options de connexion aux serveurs (PostgreSQL, Mongo),
- les répertoires du serveur web (exemple: répertoire de stockage des fichiers utilisateurs;
c'est comme cela que le serveur Python peut accéder aux fichiers mis en ligne),
- les fichiers contenant les données utilisées pour remplir la base de données,
- le niveau de logging (en production, passer en `logging.ERROR` au lieu de `logging.DEBUG`),
- des options sur les fonctions de calcul de similarités utilisées
(actuellement similarité de Jaro avec un seuil de 0.8 par défaut).

L'importation de ce fichier est quasi-systématique dans tous les modules du projet via cette directive:

    from matching_kernel import commons as cm



### Point d'entrée & API Rest

Le fichier `rest_api.py` regroupe toutes les urls que le serveur est capable de prendre en charge.
C'est l'équivalent du routeur de Symfony.
Les valeurs de retour sont toujours des chaines formatées en JSON.


#### Gestion des fichiers utilisateurs

##### Entrées


Urls:

    @app.route('/check_files_integrity/<int:experiment_id>/<string:username> \
                /<string:src_file>',
                methods=['GET'])
    @app.route('/check_files_integrity/<int:experiment_id>/<string:username> \
                /<string:src_file>/<string:dst_file>',
                methods=['GET'])
    def check_files_integrity(experiment_id=None, username=None, src_file=None, dst_file=None):

Ces urls servent uniquement à vérifier l'intégrité des fichiers uploadés par un utilisateur
lors de la création d'une expérience. Il n'y a aucune insertion de données en BDD ici.

Si la procédure se déroule correctement, l'expérience (objet `Experiment`) est persistée par Doctrine,
puis une autre fonction gérant l'insertion des données sera appelée.

!!! note "Les fonctions de Doctrine sont en général **très mal nommées**."

    Ainsi, flush() ne garantit aucune persistance des données (mais incrémente tout de même les id des clés primaires des tables...).
    L'url en question est donc appelée durant l'évènement `postPersist` de l'objet `Experiment` géré par Doctrine, ALORS QUE CET OBJET N'EST PAS ENCORE PERSISTÉ.

    Le serveur a pourtant besoin des noms des fichiers à vérifier,
    il faut donc récupérer ces fichiers via les urls
    (paramètres `src_file` et `dst_file`) car à cette étape rien n'est présent dans la base de données,
    l'expérience et les fichiers qui en dépendent ne sont pas encore persistés.


2 types de fichiers peuvent être envoyés: SBML/XML (fichier à mapper),
et un fichier contenant des données déjà mappées au format TXT/CSV.

On utilise ici le module `check_integrity` du package `input_file_parser`
pour vérifier que:

- les fichiers sont conformes (fonction `check_document_consistency`),
- les champs attendus sont présents,
- les identifiants des entités respectent la norme SBML (fonction `check_identifiers_norm`).
Concrètement il s'agit de faire respecter l'expression rationnelle suivante:
`reg_sbml_norm = re.compile('^[A-Z0-9_]*$')`

Ces vérifications sont sévères et renvoient beaucoup d'erreurs si les fichiers sont de piètre qualité.

---

Urls:

    @app.route('/upload_mapping/<int:experiment_id>/<string:dst_file>',
                methods=['GET'])
    def upload_mapping(experiment_id=None, dst_file=None):


Cette fonction permet à un utilisateur d'uploader un fichier de mapping alors que l'expérience est déjà
créée. Là encore on fait appel aux fonctions de vérification d'intégrité. Si tout se passe bien, le contenu
du fichier est inséré en BDD via la fonction `populate_database`.


En-têtes à respecter pour un fichier de type CSV:

    SOURCE  DESTINATION FORMULA ECNUMBER
    M1      M2          C6H12O6
    R1      R2                  EC1

PS: Les réactions impliquent un champ FORMULA vide tandis que les métabolites impliquent un champ ECNUMBER vide.
Des confusions à ce niveau ne feront pas planter le logiciel; si un champ est rempli à tort,
il ne sera simplement pas pris en compte.


Structure à respecter pour un fichier de type TXT:

1 seul identifiant par ligne. Cette fonction est encore expérimentale, chaque identifiant est considéré comme un métabolite (c.-à-d. qu'il n'y a pas de support des réactions ici).

---

Urls:

    @app.route('/populate_database/<int:experiment_id>',
                methods=['GET'])
    def populate_database(experiment_id=None):

Cette url est appelée ssi `check_integrity` n'a pas retourné d'erreurs;
elle gère l'import des données provenant des utilisateurs.
Elle est appelée durant l'évènement `onFlush` de l'objet `Experiment` de Doctrine;

!!! note "Pourquoi pas durant l'évènement `postFlush(PostFlushEventArgs $args)` ?"

    Comme d'habitude Doctrine s'oppose à la logique. L'évènement `postFlush` reçoit un argument
    de type `PostFlushEventArgs` qui nous permet de récupérer uniquement l'entity manager MAIS PAS
    les entités qui viennent d'être FLUSHEES. Nous n'avons donc aucun moyen d'agir sur ces entités !


On utilise ici la fonction `import_all_elements` du module `populate_database` du package `input_file_parser`.
Aucune vérification d'intégrité n'est faite ici !


Fichiers SBML:

- Insertion des métabolites (récupération des noms, identifiants, formule)
- Insertion des réactions (récupération des noms, identifiants, produits, réactifs)

PS: Si une entité (métabolite/réaction) est présente dans une association déjà validée par d'autres utilisateurs, cette association sera dupliquée et ajoutée à l'expérience courante.


Fichiers CSV & TXT:

cf recommandations dans le paragraphe précédent (url `upload_mapping`).


##### Sortie

Urls:

    @app.route('/reconstruct_sbml/<int:experiment_id>', methods=['GET'])
    def reconstruct_sbml(experiment_id=None):

Cette url permet la reconstruction d'un fichier SBML basé sur la structure du fichier de l'utilisateur,
mais dans lequel tous les identifiants ayant le statut `already matched` ont été remplacés par leurs
équivalences.

Le package `output_file_writer` est appelé ici.



#### Fonctions génériques


Urls:

    @app.route('/get_data/<int:experiment_id>/<string:status>',
                methods=['GET'])
    def get_data(experiment_id=None, status=None, sort='default', reverse=False):

Cette route permet la récupération de la liste de toutes les associations ayant un statut spécifié pour une expérience donnée.

PS: La fonction détecte la présence de multiples correspondances pour un seul identifiant.
Cette erreur sera affichée sur la page consultée.

---

Urls:

    @app.route('/vacuum')
    def vacuum():

Cette url appelle une fonction d'administration de la base de données.
Il s'agit de nettoyer les tables en supprimant tous les objets orphelins
(les entités objets biologiques qui n'appartiennent plus à des expériences).

Cette url est appelée via le bouton `vacuum` de la page d'administration.

PS: Le nettoyage prend du temps puisqu'il faut vérifier toutes les dépendances des objets.
Les objets susceptibles d'être supprimés n'encombrent pas particulièrement la base de données;
il faut plutôt les voir comme un cache destiné à des expériences ultérieures.

---

Urls:

    @app.route('/get_statistics/<int:experiment_id>',
                methods=['GET'])
    def get_statistics(experiment_id=None):

Cette url retourne le nombre d'associations dans l'expérience courante classées par statuts.
Les données servent à rafraîchir le graphique "camembert" de l'interface.



#### Fonctions de matching

Chaque fonction de matching fait appel à un module du package `matching`.

TODO: L'importation de ces modules depuis l'API n'est pas automatique mais devrait
l'être à l'avenir à l'aide du paquet Python importlib.

Urls:

    @app.route('/get_in_validation/<int:experiment_id>',
                methods=['GET'])
    def get_in_validation(experiment_id=None):

Cette url est appelée lorsqu'un utilisateur clique sur le bouton `Community matching` de l'interface.
Il s'agit de récupérer toutes les associations déjà validées par d'autres utilisateurs,
dans lesquelles une entité (métabolite/réaction) de l'expérience courante
est présente. Ces associations seront dupliquées et ajoutées à l'expérience courante.

---

Urls:

    @app.route('/validate/<int:experiment_id>',
                methods=['GET'])
    def validate(experiment_id=None):


Cette url est appelée lorsqu'un utilisateur en cliquant sur le bouton `validate` présent pour chaque
expérience, souhaite soumettre son travail à une validation de la part de la communauté.
L'expérience est supprimée de son compte et toutes les entités mappées manuellement sont soumises à une
relecture de la part des administrateurs.

---

Urls:

    @app.route('/exact_id_matching/<int:experiment_id>',
                methods=['GET'])
    def exact_id_matching(experiment_id=None):

Cette url est appelée lorsqu'un utilisateur clique sur le bouton `Reference database` de l'interface.
Il s'agit d'établir les correspondances entre les identifiants de l'expérience courante et
les identifiants présents dans la base de données de référence.

La condition *sine qua non* pour que cette fonction soit efficace
est que **les identifiants des entités concernés** doivent être intacts/non retouchés par des utilisateurs.

---

Urls:

    @app.route('/name_matching/<int:experiment_id>/<string:method> \
                /<string:biological_object_type>',
                methods=['GET'])
    def name_matching(experiment_id=None, method=None, biological_object_type=None)

Cette url est appelée lorsqu'un utilisateur clique sur le bouton `Exact matching` ou `Inferred matching` de l'interface.
Il s'agit d'établir les correspondances entre les noms des entités de l'expérience courante et
les noms des entités présents dans la base de données de référence.

Cette méthode se base sur les noms d'entités et va retourner plusieurs propositions que l'utilisateur
devra trier.

PS: Cette méthode est exclusivement dédiée au **mapping de métabolites** mais peut à terme, gérer aussi les réactions
(non prioritaire étant donné que les réactions n'ont que peu souvent des noms).


- Exact matching:
La condition *sine qua non* pour que cette fonction soit efficace
est que **les noms des entités concernées** doivent être intacts/non retouchés par des utilisateurs.

- Inferred matching:
Cette fonction établit **un score de similarité entre les noms des entités concernées et les noms déjà référencés.**

---

Urls:

    @app.route('/samir_matching/<int:experiment_id>',
                methods=['GET'])
    def samir_matching(experiment_id=None):

Cette url est appelée lorsqu'un utilisateur clique sur le bouton `Samir` de l'interface.

Cette méthode est exclusivement dédiée au mapping de réactions.

TODO: Il faudra renommer `Samir`.

---

Urls:

    @app.route('/validate_association/<int:experiment_id>/<int:assoc_id>',
                defaults={'status': cm.STA_AM},
                methods=['GET'])
    @app.route('/delete_association/<int:experiment_id>/<int:assoc_id>',
                defaults={'status': cm.STA_CANC},
                methods=['GET'])
    def modify_association(experiment_id=None, assoc_id=None, status=None):

Cette url est appelée lorsqu'un utilisateur modifie le statut d'une association.

Exemples de modifications:

    Validation:
            - proposal => already matched
            - cancelled => already matched ? (non supporté actuellement car pas d'onglet 'cancelled' sur l'interface)
    Deletion:
            - proposal => cancelled
            - already matched => cancelled


### Module convert et tests unitaires sur les fonctions de décodage-encodage d'identifiants

Commande de vérification des tests unitaires: `make unit_test`.

Les tests sont rédigés dans `/test/` et font appel au module `convert` du projet:

    from matching_kernel.convert \
    import decode_bigg, \
           decode_metacyc, \
           encode_metacyc, \
           encode_bigg, \
           universal_decoder


Le module `convert` regroupe toutes les fonctions de décodage/encodage.
Elles sont au nombre de 3 (1 par BDD supportée): Bigg, Metacyc, autres.

2 fonctions sont censées faire le travail de détection automatiquement:
`universal_decoder` et `universal_encoder`.

Tout cela a été testé sur les ensembles d'identifiants que j'ai manipulés et ça marche.
Toute modification entraînant une modification du comportement de ces fonctions sera
normalement mise en évidence par un échec des tests unitaires.

---

Le fichier `elementnames_handling.py` du module `convert` est nécessaire pour nettoyer
les entités html présentes dans les identifiants.

Tous les cas de figure rencontrés et les conversions effectuées sont listés dans
la docstring du module.

Liste des "entités" prises en charge:

    {'&pi;', '&alpha;', '&Delta;', '&mu;', '&chi;', '&plusmn;', '&tau;',
     '&DElta;', '&omega;', '&zeta;', '&gamma;', '&psi;', '&harr;', '&kappa;',
     '&lambda;', '&beta;', '&iota;', '&xi;', '&epsilon;', '&rarr;', '&nu;',
     '&delta;'}


Parfois dans les exports de Metacyc, les entités sont encodées 2 fois (l'esperluette est réencodée en `&amp;amp;`)
ce qui donne cette listé aberrante:

    {'&amp;iota;', '&amp;lambda;', '&amp;gamma;', '&amp;omega;', '&amp;pi;',
     '&amp;prime;', '&amp;mu;', '&amp;plusmn;', '&amp;tau;', '&amp;chi;',
     '&amp;delta;', '&amp;harr;', '&amp;Delta;', '&amp;kappa;', '&amp;alpha;',
     '&amp;beta;', '&amp;epsilon;', '&amp;zeta;', '&amp;rarr;', '&amp;psi;',
     '&amp;mdash;', '&amp;nu;', '&amp;xi;'}

Toutes ces données doivent être prises en charge avant l'import dans la base de données.
C'est pourquoi j'ai mentionné 3 commandes qui vont tenter de sortir les cas à gérer dans le code:

    cat all_metabolites.tbl | egrep -o --color -e '&\w+;' >> html_entities.txt
    cat metacyc_18.5.xml | egrep -o --color -e '&\w+;' >> metacyc_html_entities.txt
    cat metacyc_18.5.xml | egrep -o --color -e '&\w+;\w+;' >> metacyc_html_false_entities.txt

Ces commandes donnent les 3 fichiers mentionnés dans le chapitre [data folder & database initialisation](#data-folder-database-initialization).
Comme noté dans la docstring, la fonction `test_raw_files_and_functions()` est là pour traiter
ces fichiers et afficher une liste propre (sans les doublons) des cas à traiter.

TODO: Rendre cette fonction accessible depuis le makefile.


### Bases de données

#### Unifier

Le module `sqla_wrapper` dans le package `unifier_database` présente l'intégralité des fonctions permettant
au serveur de dialoguer avec la base de données.

On y retrouve:

- Des fonctions facilitant la connexion:
    - Context manager `SQLA_Wrapper`
    - loading_sql()
    - simple_engine()

- Des fonctions d'administration:
    - populate_default_database()
    - vaccum()
    - remove_experiments_locks()

- La définition des objets manipulés par SQLAlchemy ainsi que leurs relations:
    - ResourceFile
    - User
    - Experiment
    - Status
    - Step
    - Origin
    - Synonym
    - BiologicalObject
    - Metabolite
    - Reaction
    - Reactant
    - Association

Voir Figures 1 et 2 respectivement pour le modèle conceptuel de données et le modèle logique de données.

![Modèle Conceptuel de Données (MCD) de la base de données du projet Samifier. Nous retrouvons les entités User, Experiment, Association, Status, Step, Origin, Synonym. Un héritage par partition est
implémenté pour les entités filles SourceFile et DestinationFile héritées de l’entité mère ResourceFile, et pour les entités filles
Reaction et Metabolite héritées de l’entité mère BiologicalObject. Les relations sémantiques entre entités sont symbolisées par des associations les reliant deux à deux.](readme_ressources/mcd_samifier.png "MCD Samifier")


![Modèle Logique de Données (MLD) de la base de données du projet Samifier. Nous retrouvons les relations
correspondant aux entités et associations décrites dans le MCD. Les clés étrangères ont été ajoutées dans chaque relation ainsi que
trois nouvelles tables : metabolites_matching, reactions_matching utilisées comme index pour les méthodes d’établissement de
correspondances, et une table admin_config contenant des options de configuration de la plateforme.
](readme_ressources/mpd_samifier.png "MLD Samifier")


#### Reference database

La base de données de référence est initialisée par la commande `make pinpunt`.
Le package `reference_database` gère les insertions des données et la définition des objets manipulés.


Le module `mongo_wrapper` présente l'intégralité des fonctions permettant
au serveur de dialoguer avec la base de données.

On y retrouve:

- Des fonctions facilitant la connexion:
    - Context manager `Mongo_Wrapper`

- La définition des objets manipulés par MongoEngine ainsi que leurs relations:
    - BiologicalObject
    - Metabolite
    - Reaction

- Des fonctions permettant l'interrogation et le rapatriement des données:
    - search_document_with_database_id()
    - search_ids_with_ids()
    - search_ids_and_syns_with_names()

Ces 3 dernières fonctions permettent à toutes les méthodes de matching d'établir des correspondances.

Voir Figures 3 pour le diagramme de classes de la base de données.

![Modélisation UML du diagramme de classes des documents enregistrés dans la base de données de référence.
La classe Reaction contient des références vers des métabolites jouant les rôles de réactifs ou de produits. De nombreuses
équivalences provenant des projets MetaCyc et MetaNetX sont mémorisées dans l’attribut databases de la classe mère
BiologicalObject.
](readme_ressources/uml_mongo.png "Modélisation UML Mongo")



Lors de l'importation le module `main_importer` fait appel aux 2 modules suivants
pour gérer les fichiers en provenance de MetaCyc et de MetaNetx:

- `load_tgdb`
- `load_metanetx`


Logs produits:

    :::bash
    $ time make pinput
    INFO: Metacyc to MongoDB: 13074 reactions imported.
    INFO: Matching table metabolites: 32475 unique names.
    INFO: Matching table reactions: 3782 unique names.
    INFO: Metanetx to MongoDB: Read 65361 elements from data/chem_xref2.0.tsv.
    INFO: Metanetx to MongoDB: 11566 Metabolite updated.
    INFO: Metanetx to MongoDB: 50408 Metabolite created.
    INFO: Metanetx to MongoDB: Read 23618 elements from data/reac_xref.tsv.
    INFO: Metanetx to MongoDB: 13050 Reaction updated.
    INFO: Metanetx to MongoDB: 10305 Reaction created.
    real    13m2.001s
    user    4m29.412s
    sys     0m6.164s


PS: L'opération est longue et couteuse en ressources. Il s'agit d'insérer près de 87 000 identifiants
et de mémoriser leurs interactions. La procédure tire pleinement parti du multiprocessing disponible
sur des machines puissantes ou sur des machines virtuelles pour lesquelles on a alloué un nombre de
processeurs décent. Ainsi l'opération peut passer de la dizaine de minutes à probablement plusieurs heures
sur un système non adapté. Il en est d'ailleurs de même pour la majorité des fonctions de matching
qui ne sauraient être efficaces sur un système mono-coeur.


!!! danger "/!\ Important /!\:"

    La base de données de référence peut être considérée comme interchangeable.
    Si un jour quelqu'un décide d'utiliser un autre système que MongoDB cela sera
    toujours possible à moindre cout en respectant le format des données généré
    par les 3 fonctions vues plus haut.

    MongoDB a été choisi en raison de son cout de développement faible (**temps et
    code très restreints**). Son remplacement est donc possible, voire même conseillé.
    En effet, actuellement, **la plateforme ne supporte pas (et ne pourra jamais
    supporter) le versionning** des bases de données utilisées.

    Bien que cette fonctionnalité ait été présente dans le cahier des charges,
    les technologies employées (base de donnée de référence non orientée graphes)
    ne se prêtent pas à son implémentation de **manière propre**.

    L'utilisation du serveur virtuoso est à creuser, mais il faut aussi noter que
    RDFLib supporte PostgreSQL en tant que triple store.


\newpage

# Architecture du frontend Symfony

Le but ici n'est pas de (ré) écrire une documentation de Symfony (bien que cela ne fasse pas de mal);
il s'agit de cibler les fichiers essentiels qu'un développeur pourrait être amené à modifier.

## Cache

Commande générale pour réinitialiser les 2 caches (prod + dev) (ne pas hésiter à faire un alias...):

    sudo php /var/www/html/Symfony/bin/console cache:clear --env=prod \
    && sudo php /var/www/html/Symfony/bin/console cache:clear \
    && sudo chown www-data:www-data -R /var/www/html/Symfony/var/logs \
    && sudo chown www-data:www-data -R /var/www/html/Symfony/var/cache \
    && sudo chown www-data:www-data -R /var/www/html/Symfony/app/logs \
    && sudo chown www-data:www-data -R /var/www/html/Symfony/app/cache



## Paramètres généraux + variables globales

Fichier:

`app/config/parameters.yml`

- accès BDD (login, password, port)
- nom du projet (retrouvé dans toutes les pages générées par Twig)
- api Python (url, port)

Ce fichier est supprimé à chaque mise à jour. Les paramètres persistants doivent être placés dans:
`app/config/parameters.yml.dist`


## FOS UserBundle

Configuration du firewall:

`app/config/security.yml`

Customisation:

La customisation des formulaires se fait via la technique de l'héritage de Bundle.
Concrètement une vue placée dans NOTRE bundle sera systématiquement appelée
en lieu et place de celle par défaut de FOS (à condition que son nom et son
chemin soient identiques).

Dossier des formulaires réécrits:

`src/INRIA/UserBundle/Resources/Views/`.

## INRIA DylissBundle

### Service de dialogue avec l'API

Fichier:

`src/INRIA/DylissBundle/APIMatchingKernel/APIMatchingKernel.php`

Ce fichier contient le moyen de dialoguer avec l'API du serveur Matching Kernel.
C'est un service auquel on a accès comme ceci depuis un contrôleur:

    $this->container->get('inria_dyliss.api_matching_kernel')->ma_fonction()


TODO: à terme, pour des raisons de performances, il faudra pouvoir se passer de curl
et dialoguer exclusivement par sockets Unix.

### Routes

Fichier:

`src/INRIA/DylissBundle/Resources/config/routing.yml`

### Pages web

Assets:

`src/INRIA/DylissBundle/Resources/public/`

Vues Twig:

`src/INRIA/DylissBundle/Resources/views/`


### Contrôleurs

Dossier:

`src/INRIA/DylissBundle/Controller`

- `AdminController.php`: Page d'administration du site et de la BDD (vaccum, nb d'expériences par utilisateur, etc.).
- `ExperimentController.php`: Page de gestion des expériences pour les utilisateurs.
- `HomeController.php`: Page d'accueil.
- `MenuController.php`: Menu proposant les fonctions de matching.
- `ResultController.php`: Page proposant les onglets listant les associations par statuts.
Gestion des associations (validation, suppression, etc.) + affichage des stats.

### Gestion des évènements Doctrine

Dossier:

`src/INRIA/DylissBundle/EventListener`

Ce dossier comprend les directives à appliquer lorsque les entités Experiment et ResourceFile
sont traitées par Doctrine. C'est ici que sont appelées les fonctions de vérification d'intégrité
et d'insertion en BDD du contenu des fichiers utilisateurs.


### Formulaires

Dossier:

`src/INRIA/DylissBundle/Form`

Gestion/vérification/validation des formulaires et hydratation des objets.

La documentation de Symfony est TRÈS partielle dans ce domaine.
Les exemples sont manquants voire faux en ce qui concerne l'imbrication de formulaires avec héritage;
le générateur automatique de code est complètement perdu dans ce cas de figure pourtant
peu exotique.
Le bilan général de la création de formulaires avec Symfony est une perte de temps
potentielle qui se chiffre en dizaine d'heures; sans parler de leur conception/modification avec Twig ahurissante.

Ici: `SourceFile` est une entité spécialisée qui hérite de `ResourceFile`.
Pour traduire ce comportement il faut spécifier dans le formulaire `ResourceFileType.php`,
que ce fichier (la classe mère donc) peut être hérité...

PS: Le générateur de formulaire est complètement perdu à ce niveau;
si bien que pour qu'il fonctionne (cad génère du code erroné...),
il faut dupliquer la définition de la classe `SourceFile`
dans le fichier `src/INRIA/DylissBundle/Entity/ResourceFile.php`.



# FAQ

## Trouver où une chaine de caractères est utilisée dans le projet

Ajouter ceci dans le fichier `~/.bashrc`:

    function search() {echo "Recherche de $1 ..."; grep --color=auto -in -R "$1" ./*;}

Utilisation:

    $ search ma_chaine


## Ajout d'une méthode révolutionnaire de matching

1. Développer le module Python et le placer dans `matching_kernel/matching/`

2. Importer le module Python depuis le point d'entrée Rest, lui attribuer une route,
puis faire les vérifications qui vont bien (existence de l'expérience concernée, divers paramètres, etc.)
Fichier: `matching_kernel/rest_api.py`.

3. Mettre à jour le service Symfony `src/INRIA/DylissBundle/APIMatchingKernel/APIMatchingKernel.php`
afin qu'il dispose d'une fonction pointant directement sur le serveur Rest.

4. Ajouter une Action au contrôleur du menu de l'interface utilisateur.
Cette Action va faire des vérifications, va appeler le service APIMatchingKernel,
puis va préparer les données pour l'affichage de la vue Twig.
Fichier: `src/INRIA/DylissBundle/Controller/MenuController.php`

5. Mettre à jour le routeur Symfony.
Fichier: `app/config/routing.yml`

6. Modifier la vue Twig du menu.
Fichier: `src/INRIA/DylissBundle/Resources/views/Result/menu.html.twig`


## Vérifier les droits des dossiers et processus lancés


**NE JAMAIS utiliser chmod 777**. Ce nombre parait astucieux, mais même en situation de test
il est le signe qu'on a aucune idée de ce que l'on fait
([Ref: doc Nginx](https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/#chmod-777)).
Il faut regarder les autorisations des chemins entiers et réfléchir à ce qu'il se passe.
Pour afficher facilement toutes les autorisations sur un chemin, vous pouvez utiliser:

    namei -om /path/to/check

Example:

    :::console
    $ namei -om /root/samifier-matching-kernel/logs
    f: /root/samifier-matching-kernel/logs
    drwxr-xr-x root     root     /
    drwxr-xr-x root     root     root
    drwxr-xr-x www-data www-data samifier-matching-kernel
    drwxr-xr-x www-data www-data logs

Les droits des processus sont obtenus via cette commande:

    :::console
    $ ps aux | grep -E 'nginx|postmaster|gunicorn'
    root        ... /usr/bin/python3 /usr/local/bin/gunicorn -c ...
    root        ... nginx: master process /usr/sbin/nginx -g daemon off;
    postgres    ... /usr/lib/postgresql/9.4/bin/postmaster --config_file ...
    www-data    ... nginx: worker process
    www-data    ... nginx: worker process
    www-data    ... nginx: worker process
    www-data    ... nginx: worker process
    www-data    ... /usr/bin/python3 /usr/local/bin/gunicorn -c ...
    www-data    ... /usr/bin/python3 /usr/local/bin/gunicorn -c ...


# TODO

- Captcha à l'inscription:

https://github.com/excelwebzone/EWZRecaptchaBundle

- Téléchargement sécurisé:

nginx module http://nginx.org/en/docs/http/ngx_http_secure_link_module.html

- Masquer tous les chemins et les erreurs de debug:

Concrètement le serveur Python devrait envoyer les noms des exceptions mais pas le message entier.


# Resources

## Liens

* [Web Server Gateway Interface](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface).
* [Qu’est-ce que WSGI et à quoi ça sert ?](http://sametmax.com/quest-ce-que-wsgi-et-a-quoi-ca-sert/)

## Outils conseillés

* [Robomongo](https://robomongo.org/): Navigation et administration d'une base de données Mongo.
* [pgAdmin III](https://www.pgadmin.org/): (dans les dépôts) Navigation et administration d'une base de données PostgreSQL.
* [Navicat](https://www.navicat.com/fr): (propriétaire, payant) Navigation et administration de toute base de données à l'exclusion de Mongo.
* [Eric IDE](http://eric-ide.python-projects.org/): IDE Python.
* [KDevelop](https://www.kdevelop.org/): IDE générique (PHP entre autres).


*[WSGI]: Web Server Gateway Interface
*[JSON]: JavaScript Object Notation
