/*
Navicat PGSQL Data Transfer

Source Server         : symfony
Source Server Version : 90406
Source Host           : localhost:5432
Source Database       : symfony
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90406
File Encoding         : 65001

Date: 2016-03-24 19:36:24
*/

-- ----------------------------
-- Sequence structure for experiment_id_seq
-- ----------------------------
-- DROP SEQUENCE "public"."experiment_id_seq";
CREATE SEQUENCE "public"."experiment_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."experiment_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for resource_file_id_seq
-- ----------------------------
-- DROP SEQUENCE "public"."resource_file_id_seq";
CREATE SEQUENCE "public"."resource_file_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."resource_file_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
-- DROP SEQUENCE "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."users_id_seq"', 3, true);

-- ----------------------------
-- Table structure for admin_config
-- ----------------------------
-- DROP TABLE admin_config;

CREATE TABLE admin_config
(
  id int4 NOT NULL,
  number_experiments integer NOT NULL,
  CONSTRAINT admin_config_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


-- ----------------------------
-- Table structure for experiment
-- ----------------------------
-- DROP TABLE IF EXISTS "public"."experiment";
--CREATE TABLE IF NOT EXISTS "public"."experiment" (
--"id" int4 NOT NULL,
--"user_id" int4 NOT NULL,
--"src_file_id" int4 NOT NULL,
--"dst_file_id" int4,
--"date" timestamp NOT NULL,
--"dst_origin_id" int4 NOT NULL,
--"src_origin_id" int4 NOT NULL,
--)
--WITH (OIDS=FALSE)
--;

-- ----------------------------
-- Table structure for resource_file
-- ----------------------------
--DROP TABLE IF EXISTS "public"."resource_file";
CREATE TABLE IF NOT EXISTS "public"."resource_file" (
"id" int4 NOT NULL,
"real_name" varchar(255) COLLATE "default" NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"md5_checksum" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"upload_date" timestamp NOT NULL,
"type" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
"id" int4 NOT NULL,
"username" varchar(255) COLLATE "default" NOT NULL,
"username_canonical" varchar(255) COLLATE "default" NOT NULL,
"email" varchar(255) COLLATE "default" NOT NULL,
"email_canonical" varchar(255) COLLATE "default" NOT NULL,
"enabled" bool NOT NULL,
"salt" varchar(255) COLLATE "default" NOT NULL,
"password" varchar(255) COLLATE "default" NOT NULL,
"last_login" timestamp DEFAULT NULL::timestamp without time zone,
"locked" bool NOT NULL,
"expired" bool NOT NULL,
"expires_at" timestamp DEFAULT NULL::timestamp without time zone,
"confirmation_token" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"password_requested_at" timestamp DEFAULT NULL::timestamp without time zone,
"roles" text COLLATE "default" NOT NULL,
"credentials_expired" bool NOT NULL,
"credentials_expire_at" timestamp DEFAULT NULL::timestamp without time zone
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."users"."roles" IS '(DC2Type:array)';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('1', 'user', 'user', 'test@mail.com', 'test@mail.com', 't', '1zuo0fdvj2ck8g8wks8gscs8g04co0w', 'AFLqma5tBQ5GN3G0lE2sJyj6llAb4VNPEs1RPiFVzbpxTrmBrDsNVy2u9eluhjhsDF+DRs9cr/1oGuJJGw7dMQ==', '2016-03-23 22:17:14', 'f', 'f', null, null, null, 'a:0:{}', 'f', null);
INSERT INTO "public"."users" VALUES ('3', 'admin', 'admin', 'admin@test.com', 'admin@test.com', 't', 'ok72jkxq7ms0g4kso40sog8o08sgcw4', 'bwk6eudAo6kuc1ZWwwJdApA/gLOA1vsPxam1aPFTXyhaLIdmGsw7TmqoehSpkg8hWL5bmBU88fS+b2rSezwCXg==', null, 'f', 'f', null, null, null, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 'f', null);

-- ----------------------------
-- Records of admin_config
-- ----------------------------
INSERT INTO "public"."admin_config" VALUES (1, 2);

-- ----------------------------
-- Indexes structure for table experiment
-- ----------------------------
--CREATE INDEX "idx_136f58b2a76ed395" ON "public"."experiment" USING btree (user_id);
--CREATE UNIQUE INDEX "uniq_136f58b219b42aa2" ON "public"."experiment" USING btree (src_file_id);
--CREATE UNIQUE INDEX "uniq_136f58b2b91abf6c" ON "public"."experiment" USING btree (dst_file_id);

-- ----------------------------
-- Primary Key structure for table experiment
-- ----------------------------
--ALTER TABLE "public"."experiment" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table resource_file
-- ----------------------------
ALTER TABLE "public"."resource_file" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE UNIQUE INDEX "uniq_1483a5e992fc23a8" ON "public"."users" USING btree (username_canonical);
CREATE UNIQUE INDEX "uniq_1483a5e9a0d96fbf" ON "public"."users" USING btree (email_canonical);
CREATE INDEX "roles_idx" ON "public"."users" USING btree (roles COLLATE pg_catalog."default" text_pattern_ops);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."experiment"
-- ----------------------------
--ALTER TABLE "public"."experiment" ADD FOREIGN KEY ("dst_file_id") REFERENCES "public"."resource_file" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
--ALTER TABLE "public"."experiment" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
--ALTER TABLE "public"."experiment" ADD FOREIGN KEY ("src_file_id") REFERENCES "public"."resource_file" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
