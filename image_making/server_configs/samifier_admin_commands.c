//doc execve: http://manpagesfr.free.fr/man/man2/execve.2.html

/* EXIT_SUCCESS, EXIT_FAILURE */
#include <stdlib.h>
#include <unistd.h>
/* perror(), errno */
#include <errno.h>
/* fprintf() */
#include <stdio.h>
/* pid_t */
#include <sys/types.h>
/* wait() */
#include <sys/wait.h>
#include <string.h>
#include <assert.h>

#define RED         "\033[91m"
#define BLUE        "\033[36m"
#define COLOR_RESET "\033[0m"

#define DATABASE_NAME "symfony"
#define ADMIN_NAME    "postgres"
#define USER_NAME     "lex"
#define BACKUP_DIR    "/var/www/html/Symfony/web/uploads/admin/backups/"

#define RESTART_MATCHING_KERNEL "restart_matching_kernel"
#define BACKUP_POSTGRESQL       "backup_postgresql"
#define BACKUP_MONGO            "backup_mongo"
#define BACKUP_WEB              "backup_web"
#define RESTORE_POSTGRESQL      "restore_postgresql"
#define RESTORE_MONGO           "restore_mongo"
#define RESTORE_WEB             "restore_web"
#define TEST                    "test"

extern int errno;

int check_valid_action(char *action_name) {
    /* Return 0 if action_name is not found in allowed actions,
     * return 1 otherwise.
     */

    char *allowed_names [] = {
        RESTART_MATCHING_KERNEL,
        BACKUP_POSTGRESQL,
        BACKUP_MONGO,
        BACKUP_WEB,
        RESTORE_POSTGRESQL,
        RESTORE_MONGO,
        RESTORE_WEB,
        TEST,
    };
    int len = sizeof(allowed_names)/sizeof(allowed_names[0]);

    for(int i = 0; i < len; i++)
    {
        if(strcmp(allowed_names[i], action_name) == 0)
            // found => action accepted
            return 1;
    }
    return 0;
}

int check_exit_status(long *exit_status_code)
{
    /* Check error code of execv functions.
     * If a problem occurs, the program ends up.
     *
     * Note: Don't forget to use this function after each call of execv().
     */

    if (*exit_status_code == -1) {
        //perror("execv");
        fprintf(stderr, "Error: %d - %s\n", errno, strerror(errno));
        exit(errno);
    }
    return 0;
}

int subv(char *path, char * const argv[])
{
    /* This fork doesn't return to the parent process until the child
     * has completed its task.
     * It returns -1 in case of fork error,
     * otherwise the child returns execv() status code.
     */
    int ret;
    pid_t a_pid;
    // fork() can fail, so we handle this as long
    // as the error is EAGAIN
    do {
        a_pid = fork();
    } while ((a_pid == -1) && (errno == EAGAIN));

    if (a_pid == -1)
        // Fork error
        return -1;
    if (a_pid != 0) {
        // Wait the end of the child in the parent
        // ret is the status code of the child process
        // Without this, the end of the parent will
        // create a zombie process.
        wait(&ret);
        return ret;
    }
    assert(a_pid == 0);
    // Here a_pid == 0, we are in the child process
    return execv(path, argv);
}

int main (int argc, char **argv) {
    /*
     * execv() is used to replace the current program by another without changing
     * the current PID.
     * A new clean environment is passed to the child process.
     *
     * Params:
     * 1: absolute filepath or filename to be executed
     * 2: list of arguments passed to the executable
     *     - executable filename
     *     - last element: NULL pointer or (void *)0 because NULL
     *     is not always replaced by a const pointer.
     * 3: list or array of environment pointers
     *
     * Note: We DO NOT USE <system()> function which exposes to many
     * security vulnetabilities.
     *
     * Note: Since execv() replace the current program by another command,
     * the exit code provided by this program is the exit code of the
     * command unless an error occurs with execv().
     *
     * Note: When multiple calls to execv() are made, only the first one
     * is executed if we don't do a fork to execute the process in a child
     * process. This fork doesn't return to the parent process until the child
     * has completed its task.
     *
     * Compilation & usage:
     * gcc -std=c11 -o samifier_admin_commands samifier_admin_commands.c
     * chown root samifier_admin_commands
     * chmod u=rwx,go=xr,+s samifier_admin_commands
     * mv samifier_admin_commands /usr/bin
     */

    /* setuid(ID) is equivalent to setreuid(ID, ID) where setreuid()
     * sets the UID and the EUID like this: setreuid(UID, EUID).
     * By default UID is the uid of the user that launch the program (www-data).
     * EUID is the UID fixed by SUID bit with chmod u=rwx,go=xr,+s.
     * by default a program has as UID its EUID (the UID of the user that
     * created it).
     */
     /*
     int uid=getuid();
     int euid=geteuid();
     printf ("UID %d - EUID %d\n", uid, euid);
     setuid(euid, euid);
     //or: setuid(0);
     */
    // Set EUID fixed by sticky bit, to UID, allows us to run this program
    // as root without sudo.
    setuid(geteuid());

    if ((argc < 2) | (check_valid_action(argv[1]) == 0)) {
        fprintf(stdout, "USAGE: /usr/bin/samifier_admin_commands action\n"
            "action could be only:\n"
            RESTART_MATCHING_KERNEL "\n"
            BACKUP_POSTGRESQL "\n"
            BACKUP_MONGO "\n"
            BACKUP_WEB "\n"
            RESTORE_POSTGRESQL "\n"
            RESTORE_MONGO "\n"
            RESTORE_WEB "\n");
        return EXIT_FAILURE;
    }
    char *action_name = argv[1];
    // Error code of execv()
    long ret = 0;
    printf("Action: %s\n", action_name);

    if (!strcmp(action_name, RESTART_MATCHING_KERNEL)) {
        printf(BLUE "Restarting matching-kernel" COLOR_RESET "\n");

        char *arguments[] = {"service",
            "matching-kernel", "restart", NULL};
        ret = execv("/usr/sbin/service", arguments);
    }

    if (!strcmp(action_name, BACKUP_POSTGRESQL)) {
        printf(BLUE "Backup PostgreSQL database..." COLOR_RESET "\n");
        // pg_dump --username=postgres -Fc symfony --file=postgres_save

        char *arguments[] = {"pg_dump",
            "--username=" ADMIN_NAME, "-Fc", DATABASE_NAME, "--file=" BACKUP_DIR "postgres_save", NULL};
        ret = execv("/usr/bin/pg_dump", arguments);
    }

    if (!strcmp(action_name, BACKUP_MONGO)) {
        printf(BLUE "Backup MongoDB database..." COLOR_RESET "\n");
        // mongodump --db unifier --collection biological_object --gzip --out backup/

        char *arguments[] = {"mongodump",
            "--db", "unifier", "--collection", "biological_object", "--gzip", "--out", BACKUP_DIR, NULL};
        ret = execv("/usr/bin/mongodump", arguments);
    }

    if (!strcmp(action_name, BACKUP_WEB)) {
        printf(BLUE "Backup user files..." COLOR_RESET "\n");
        // tar cvf backup/backup_user_files.tar /var/www/html/Symfony/web/uploads

        char *arguments[] = {"tar",
            "cvf", BACKUP_DIR "backup_user_files.tar", "/var/www/html/Symfony/web/uploads", NULL};
        ret = execv("/bin/tar", arguments);
    }

    if (!strcmp(action_name, RESTORE_POSTGRESQL)) {
        printf(BLUE "Restore PostgreSQL database..." COLOR_RESET "\n");

        printf(BLUE "Disconnect users" COLOR_RESET "\n");
        // psql --username postgres -c "REVOKE CONNECT ON DATABASE symfony FROM public; SELECT pg_terminate_backend(pid) FROM pg_stat_activity where pid <> pg_backend_pid();"
        char *arguments[] = {"psql",
            "--username", ADMIN_NAME, "-c", "REVOKE CONNECT ON DATABASE " DATABASE_NAME " FROM public; SELECT pg_terminate_backend(pid) FROM pg_stat_activity where pid <> pg_backend_pid();", NULL};
        ret = subv("/usr/bin/psql", arguments);
        check_exit_status(&ret);

        printf(BLUE "Database deletion" COLOR_RESET "\n");
        // psql --username postgres -c "DROP DATABASE IF EXISTS symfony;"
        char *arguments_2[] = {"psql",
            "--username", ADMIN_NAME, "-c", "DROP DATABASE IF EXISTS " DATABASE_NAME ";", NULL};
        ret = subv("/usr/bin/psql", arguments_2);
        check_exit_status(&ret);

        printf(BLUE "Database creation" COLOR_RESET "\n");
        // createdb --username=postgres -O lex --template=template0 -E UTF8 "symfony"
        char *arguments_3[] = {"createdb",
            "--username=" ADMIN_NAME, "-O", USER_NAME, "--template=template0", "-E", "UTF8", DATABASE_NAME, NULL};
        ret = subv("/usr/bin/createdb", arguments_3);
        check_exit_status(&ret);

        printf(BLUE "Allocation of rights" COLOR_RESET "\n");
        // psql --username postgres -c "GRANT TEMPORARY ON DATABASE symfony TO public; GRANT ALL ON DATABASE symfony TO lex;"
        char *arguments_4[] = {"psql",
            "--username", ADMIN_NAME, "-c", "GRANT TEMPORARY ON DATABASE " DATABASE_NAME " TO public; GRANT ALL ON DATABASE " DATABASE_NAME " TO " USER_NAME ";", NULL};
        ret = subv("/usr/bin/psql", arguments_4);
        check_exit_status(&ret);

        printf(BLUE "Database restoration" COLOR_RESET "\n");
        // pg_restore --username=postgres -d symfony postgres_save
        char *arguments_5[] = {"pg_restore",
            "--username=" ADMIN_NAME, "-d", DATABASE_NAME, BACKUP_DIR "postgres_save", NULL};
        ret = subv("/usr/bin/pg_restore", arguments_5);
        check_exit_status(&ret);

        printf(BLUE "Restarting matching-kernel" COLOR_RESET "\n");
        char *arguments_6[] = {"service",
            "matching-kernel", "restart", NULL};
        ret = subv("/usr/sbin/service", arguments_6);
    }

    if (!strcmp(action_name, RESTORE_MONGO)) {
        printf(BLUE "Restore MongoDB database..." COLOR_RESET "\n");
        // mongo unifier --eval "db.dropDatabase()"
        // mongorestore --db unifier --collection biological_object --gzip backup/unifier/biological_object.bson.gz
        char *arguments[] = {"mongo",
            "unifier", "--eval", "db.dropDatabase()", NULL};
        ret = subv("/usr/bin/mongo", arguments);
        check_exit_status(&ret);

        char *arguments_2[] = {"mongorestore",
            "--db", "unifier", "--collection", "biological_object", "--gzip", BACKUP_DIR "unifier/biological_object.bson.gz", NULL};
        ret = subv("/usr/bin/mongorestore", arguments_2);
    }

    if (!strcmp(action_name, RESTORE_WEB)) {
        printf(BLUE "Restore user files..." COLOR_RESET "\n");
        // tar xvf backup/backup_user_files.tar -C /
        char *arguments[] = {"tar",
            "xvf", BACKUP_DIR "backup_user_files.tar", "-C", "/", NULL};
        ret = execv("/bin/tar", arguments);
    }
       if (!strcmp(action_name, TEST)) {
        printf(BLUE "Test fork..." COLOR_RESET "\n");
        char *arguments[] = {"sleep",
            "3", NULL};
        ret = subv("/bin/sleep", arguments);
        printf("Command 1 ends up: %d\n", ret);
        char *arguments2[] = {"sleep",
            "2", NULL};
        ret = subv("/bin/sleep", arguments2);
        printf("Command 2 ends up: %d\n", ret);
    }

    // Last check of ret value
    check_exit_status(&ret);
    return EXIT_SUCCESS;
}

