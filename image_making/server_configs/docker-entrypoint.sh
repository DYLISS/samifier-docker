#!/bin/bash
# This script is used to initialize the postgresql database
# If this job is already done, it runs the given executable (with it's parameters)
set -e

# Update rights
mkdir -p "$PGDATA"
chmod 700 "$PGDATA"
chown -R postgres "$PGDATA"

chmod g+s /run/postgresql
chown -R postgres /run/postgresql


# look specifically for PG_VERSION, as it is expected in the DB dir
# Created during init :
# This condition is executed only on the first run
if [ ! -s "$PGDATA/PG_VERSION" ]; then

# Server init / cluster / templates
eval "gosu postgres /usr/lib/postgresql/9.4/bin/initdb $POSTGRES_INITDB_ARGS"

# Blank password !
pass=

# internal start of server in order to allow set-up using psql-client
# does not listen on TCP/IP and waits until start finishes
gosu postgres /usr/lib/postgresql/9.4/bin/pg_ctl -D "$PGDATA" \
    -o "-c listen_addresses=''" \
    -w start

: ${POSTGRES_USER:=postgres}
: ${POSTGRES_DB:=$POSTGRES_USER}
export POSTGRES_USER POSTGRES_DB

# Stop on errors
psql=( psql -v ON_ERROR_STOP=1 )

# Create database
gosu postgres createdb -O postgres --template=template0 -E UTF8 "$POSTGRES_DB"

# Create user
if [ "$POSTGRES_USER" = 'postgres' ]; then
    op='ALTER'
else
    op='CREATE'
fi
"${psql[@]}" --username postgres <<-EOSQL
    $op USER "$POSTGRES_USER" WITH SUPERUSER $pass ;
EOSQL
echo

# Give rights on the database
psql+=( --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" )

# Auto load all sh scripts or sql dumps in this directory
echo
for f in /docker-entrypoint-initdb.d/*; do
    case "$f" in
        *.sh)     echo "$0: running $f"; . "$f" ;;
        *.sql)    echo "$0: running $f"; "${psql[@]}" < "$f"; echo ;;
        *.sql.gz) echo "$0: running $f"; gunzip -c "$f" | "${psql[@]}"; echo ;;
        *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

# Stop server
gosu postgres /usr/lib/postgresql/9.4/bin/pg_ctl -D "$PGDATA" -m fast -w stop

# Install all projects
cd /root/ && make install

echo
echo -e "\033[91mSamifier init process complete; ready for start up. You can reboot the container.\033[0m"
echo
fi

# Load offcial/finalized server
# If the first config is already done, it is the unique executed command
echo "Run $@"
exec "$@"


