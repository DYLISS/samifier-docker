
all:
	@echo Generating pdf...
	@pandoc README.md --number-section --latex-engine=xelatex --template=readme_ressources/mytemplate.tex --listings --toc -o README.pdf
